FROM node:9-slim
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get install -y sqlite3 libsqlite3-dev
RUN npx knex migrate:latest
EXPOSE 3333
CMD [ "npm", "start" ]
