import Knex from 'knex';

export async function up(knex: Knex) {
    // Criar a tabela
    await knex.schema.createTable('items', (table) => {
        table.increments('id').primary();
        table.string('image').notNullable();
        table.string('title').notNullable();
    });
}

export async function down(knex: Knex) {
    // Volta atrás (deleta a tabela)
    await knex.schema.dropTable('items');
}