import express from 'express';
import path from 'path';
import cors from 'cors';
import routes from './routes';
import { errors } from 'celebrate'

// cria a pasta com o nome do projeto
// mkdir <nome_do_projeto>
// acessa a pasta do projeto
// cd <nome_do_projeto>
// npm init
// npm install express
// npm install @types/express -D
// npm install ts-node -D
// npm install typescript -D
// npx tsc --init
// npx ts-node src/server.ts
// npm install ts-node-dev -D
// npx ts-node-dev src/server.ts
// depois de colocar o comando anterior na tag de scripts no arquivo 'package.json'
// npm run dev
// npm install knex
// npm install sqlite3
// npx knex migrate:latest --knexfile knexfile.ts migrate:latest
// depois de colocar o comando anterior na tag de scripts no arquivo 'package.json'
// npm run knex:migrate
// adicionado novo script para rodar as seeds e retirado "migrate:latest" do comando anterior no script
// "knex:seed": "knex --knexfile knexfile.ts seed:run"
// adicionado no script de 'dev' no 'package.json' as flags '--transpileOnly --ignore-watch node_modules' para toda vez que reinstart o servidor ignorar a pasta node_modules porque não tem alterações nela
// npm install cors
// npm install @types/cors

const app = express();

app.use(cors());
app.use(express.json());

app.use(routes);

app.use('/uploads', express.static(path.resolve(__dirname, '..', 'uploads')));

// GET: Busca uma ou mais informações do back-end
// POST: Criar uma infromação no back-end
// PUT: Atualiza uma informação existente no back-end
// DELETE: Remove uma informação do back-end

// GET: http://localhost:3333/users = Lista usuários
// GET: http://localhost:3333/users = Buscar dados do usuário 5
// POST: http://localhost:3333/users = Criar um usuário

// Resquest Params: parâmetros que vem pela própria rota que identificam um recurso
// Resquest Query Params: parâmetros que vem pela própria rota que identificam um recurso
// Resquest Body: Parâmeteros para criação/atualização de informações

// SELECT * FROM users WHERE name = 'Diego'
// knex('users').where('name', 'Diego').select('*')

// const users = [
//     'Diego',
//     'Cleiton',
//     'Robson',
//     'Daniel'
// ];

// app.get('/users', (request, response) => {
//     const search = String(request.query.search);

//     const filteredUsers = search ? users.filter(user => user.includes(search)) : users;

//     return response.json(filteredUsers);
// });

// app.get('/users/:id', (request, response) => {
//     const id = Number(request.params.id);

//     const user = users[id];

//     return response.json(user);
// });

// app.post('/users', (request, response) => {
//     const data = request.body;

//     const user = {
//         name: data.name,
//         email: data.email
//     };

//     return response.json(user); 
// })

app.use(errors());

app.listen(3333);



